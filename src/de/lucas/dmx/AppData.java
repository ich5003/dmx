package de.lucas.dmx;// Ende Attribute

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * This class saves variables needed by the whole program
 */
class AppData {
  //Default path (used to save Configurations)
  static final String configPath = System.getenv("USERPROFILE") + File.separator + "DMX";
  //Telnet telnet: Connection
  static volatile Telnet telnet;
  //List of devices
  static List<Device> devices = new ArrayList<>();
}