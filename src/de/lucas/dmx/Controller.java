package de.lucas.dmx;

import de.lucas.dmx.listener.OnControllerListener;

/**
 * This thread is used to control other threads
 */
class Controller extends Thread {
    //can be set to false if Thread is stopped intentionally
    boolean running = true;
    private OnControllerListener callback;
    //Thread to control
    private Thread toControl;

    /**
     * @param callback  Called when a thread dies
     * @param toControl This thread is to be controlled
     */
    Controller(OnControllerListener callback, Thread toControl) {
        this.callback = callback;
        this.toControl = toControl;
    }

    /**
     * Called when starting the thread
     */
    @Override
    public void run() {
        while (running) {
            if (!toControl.isAlive()) {
                /*
                Thread died.
                Calling callback.threadDied() to notify.
                 */
                callback.threadDied();
            }
            try {
                /*
                Controller is not a high priority thread.
                Pausing this thread to save performance
                 */
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}