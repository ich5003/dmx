package de.lucas.dmx;

import de.lucas.dmx.listener.OnDevicePositionChangeListener;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Handles all device specific actions
 */
public class Device extends JFrame {
    //Static values (used to understand the code better)
    static final int
            CVC = 0,
            CV = 1;
    static final int xAxis = 1, yAxis = 2, zAxis = 3;


    //Default device values (from DMX-Control)
    int position; //Device position in DMX control
    String name; //Name of the device
    int startAdress, endAdress; //Start and End DMX-address
    int panChannel = -1, tiltChannel = -1, panFineChannel = -1, tiltFineChannel = -1; //Pan and tilt channels (-1 is default and means the specific device does not have a pan or a tilt channel)


    //Used by other classes (f.e. Startscreen) to make sure a device is ready
    boolean isSetup = false;
    OnDevicePositionChangeListener onDevicePositionChangeListener;
    //Created by Device.form (GUI elements)
    private JPanel panel1;
    private JRadioButton rotXRadiobutton;
    private JRadioButton rotYRadiobutton;
    private JRadioButton rotZRadiobutton;
    private JTextField degreeField;
    private JButton saveButton;
    private JTextField xPosField, yPosField, zPosField;
    private JButton loadButton;
    private JTextField panRotField;
    private JTextField tiltRotField;
    private JList rotationsList;
    private DefaultListModel model;
    private JButton addRotation;
    private JTextField formulaAlphaTextField;
    private JTextField formulaBetaTextField;
    //Default device values (indirectly from dmx control; see method checkInverted)
    private boolean invertedPan = false; //True means the device is Pan-inverted in DMX-control
    private boolean invertedTilt = false; //True means the device is Tilt-inverted in DMX-control
    //Default devices values (from Device Settings)
    private int xPos, yPos, zPos;
    private List<Integer> rotationAxis = new ArrayList<>();
    private List<Integer> rotationDegrees = new ArrayList<>();
    private int panDegree, tiltDegree;
    private int[][] rotationMatrix = new int[3][3];
    private Expression alphaExpression = new ExpressionBuilder("a").variable("a").build(), betaExpression = new ExpressionBuilder("b").variable("b").build();
    private String alphaString, betaString;
    //Used by checkInverted()
    private boolean checkingInvertPan = false, checkingInvertTilt = false;
    private int invertPanCheckNumber = 0;
    private int invertTiltCheckNumber = 0;
    //Used to move a device from and old position into a specific direction
    //See forward(); backward(); left(); right();
    //volatile is necessary because key presses in DeviceControl run commands in a new Thread. Positions have to be synced in all threads
    private volatile int[] lastPos = new int[3];

    /**
     * Creates the device's setting window and sets values
     *
     * @param position Device position in DMX control
     * @param name     Device name
     */
    public Device(int position, String name) {
        super(name);

        this.position = position;
        this.name = name;

        //Set window options
        setSize(1000, 250);
        setType(Type.UTILITY);
        setContentPane(panel1);
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setVisible(false);

        //Get model from the JList (used for multiple rotation)
        rotationsList.setModel(new DefaultListModel());
        model = (DefaultListModel) rotationsList.getModel();

        //Save configuration when user clicks the button
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    //Set entered variables
                    xPos = Integer.valueOf(xPosField.getText());
                    yPos = Integer.valueOf(yPosField.getText());
                    zPos = Integer.valueOf(zPosField.getText());


                    panDegree = Integer.valueOf(panRotField.getText());
                    tiltDegree = Integer.valueOf(tiltRotField.getText());

                    alphaString = formulaAlphaTextField.getText();
                    betaString = formulaBetaTextField.getText();

                    //Parsing formula to a Expression
                    alphaExpression = new ExpressionBuilder(alphaString)
                            .variable("a")
                            .build();
                    betaExpression = new ExpressionBuilder(betaString)
                            .variable("b")
                            .build();


                    doSave();

                    //Notifies the user on successful save
                    JOptionPane.showConfirmDialog(null,
                            "Einstellungen wurden gespeichert.",
                            "Gespeichert",
                            JOptionPane.DEFAULT_OPTION);
                } catch (NumberFormatException numberFormatException) {
                    //Notifies a user on invalid values
                    numberFormatException.printStackTrace();
                    JOptionPane.showConfirmDialog(null,
                            "Mindestens einer der eingegebenen Werte ist ungültig. Bitte Stelle sicher, dass nur Zahlen eingegeben wurden.",
                            "Ungültige Eingabe",
                            JOptionPane.DEFAULT_OPTION);
                }
            }
        });

        //Load configuration from another device when user clicks the button
        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Get a list of all device configurations
                File devicesDir = new File(AppData.configPath + "/Devices");
                File[] devices = devicesDir.listFiles();
                assert devices != null;
                String[] devicesStrings = new String[devices.length];
                for (int i = 0; i < devices.length; i++) {
                    devicesStrings[i] = devices[i].getName().replace(".ini", "");
                }
                //Show a GUI to chose the device
                String s = (String) JOptionPane.showInputDialog(
                        null,
                        "Bitte wähle das Gerät aus, von dem die Einstellungen importiert werden sollen.",
                        "Einstellungen importieren",
                        JOptionPane.PLAIN_MESSAGE,
                        null,
                        devicesStrings,
                        null);
                //Import settings
                for (int i = 0; i < devices.length; i++) {
                    if (devicesStrings[i].equals(s)) {
                        setup(true, devices[i]);
                    }
                }
            }
        });

        //Called to add a rotation to the rotation list
        addRotation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (rotXRadiobutton.isSelected()) rotationAxis.add(xAxis);
                if (rotYRadiobutton.isSelected()) rotationAxis.add(yAxis);
                if (rotZRadiobutton.isSelected()) rotationAxis.add(zAxis);

                rotationDegrees.add(Integer.parseInt(degreeField.getText().trim()));

                degreeField.setText("");

                updateListModel(false);
            }
        });

        //Called to remove rotations from rotation list on double click
        rotationsList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (e.getClickCount() == 2) {
                    int index = rotationsList.locationToIndex(e.getPoint());
                    int reply = JOptionPane.showConfirmDialog(null,
                            "Soll diese Drehung wirklich gelöscht werden?",
                            "Sicher?",
                            JOptionPane.YES_NO_OPTION);
                    if (reply == JOptionPane.NO_OPTION)
                        return;
                    rotationDegrees.remove(index);
                    rotationAxis.remove(index);
                    updateListModel(false);
                }
            }
        });
    }


    /**
     *
     * @param panChannel pan channel of the device
     * @param tiltChannel tilt channel of the device
     */
    void setPanTiltChannel(int panChannel, int tiltChannel) {
        setPanTiltChannel(panChannel, tiltChannel, -1, -1);
    }

    /**
     *
     * @param panChannel pan channel of the device
     * @param tiltChannel tilt channel of the device
     * @param panFineChannel pan fine channel of the device
     * @param tiltFineChannel tilt fine channel of the device
     */
    void setPanTiltChannel(int panChannel, int tiltChannel, int panFineChannel, int tiltFineChannel) {
        //saves Pan- and Tilt-Channel
        this.panChannel = panChannel + startAdress;
        this.tiltChannel = tiltChannel + startAdress;
        this.panFineChannel = panFineChannel + startAdress;
        this.tiltFineChannel = tiltFineChannel + startAdress;

        //Load current values of the pan and tilt channels to get the position of the device
        AppData.telnet.send("GetChannel " + this.panChannel);
        AppData.telnet.send("GetChannel " + this.tiltChannel);
        AppData.telnet.send("GetChannel " + this.panFineChannel);
        AppData.telnet.send("GetChannel " + this.tiltFineChannel);
    }


    /**
     * When DMX control sends an update of DMX channels
     *
     * @param channel DMX channel
     * @param value DMX value
     * @param type DMX input type (CV or CVC)
     *             NOTE:
     *             DMX's CVC outputs are the real numbers.
     *             DMX's CV outputs are the inverted numbers. (only for inverted devices)
     */
    void update(int channel, int value, int type) {
        //This is used when setting up the device to identify whether the device is inverted or not.
        if (checkingInvertPan || checkingInvertTilt) {
            checkInvertResponse(channel, value, type);
        }

        /*
        Check if the device uses the specific channel.
        If the device is inverted in DMX-Control invert the number if(type==CVC && invertedPan)

        NOTE:
        DMX's CV outputs are the real numbers.
        DMX's CVC outputs are the inverted numbers. (only for inverted devices)

        As DMX-control inverts inputs automatically, this program uses the inverted numbers.
         */
    }


    /**
     * Move device forward
     */
    void forward() {
        int[] tempLastPos = lastPos;
        tempLastPos[0] += 50;
        moveTo(tempLastPos);
    }

    /**
     * Move device backward
     */
    void backward() {
        int[] tempLastPos = lastPos;
        tempLastPos[0] -= 50;
        moveTo(tempLastPos);
    }

    /**
     * Move device left
     */
    void left() {
        int[] tempLastPos = lastPos;
        tempLastPos[1] += 50;
        moveTo(tempLastPos);
    }

    /**
     * Move device right
     */
    void right() {
        int[] tempLastPos = lastPos;
        tempLastPos[1] -= 50;
        moveTo(tempLastPos);
    }


    /**
     * Converts room coordinates to device coordinates and then calculates alpha and beta
     *
     * @param roomCoordinates coordinates in the room
     * @see Maths
     */
    void moveTo(int[] roomCoordinates) {
        //Converting room coordinates to device coordinates
        int[] deviceCoordinates = Maths.newCoordinates(xPos, yPos, zPos, roomCoordinates, rotationMatrix);
        //Save room coordinates (used to move a device from on old to a new position
        lastPos = roomCoordinates;

        //Calculate alpha and beta
        double
                alpha = Maths.getAlpha(deviceCoordinates),
                beta = Maths.getBeta(deviceCoordinates);


        if (deviceCoordinates[0] < 0)
            alpha += 180;

        //Calculate real beta using entered formula
        alphaExpression.setVariable("a", alpha);
        betaExpression.setVariable("b", beta);

        double
                newAlpha = alphaExpression.evaluate(),
                newBeta = betaExpression.evaluate();

        /*
        Outdated:
        int panDmx = Maths.degreeToDmx(alpha + 360d, panDegree);
        int tiltDmx = Maths.degreeToDmx(Math.abs(beta - 90), tiltDegree);
        */

        //Convert Degrees to DMX values
        int
                panDmxEvaluated = Maths.degreeToDmx(newAlpha, panDegree),
                tiltDmxEvaluated = Maths.degreeToDmx(newBeta, tiltDegree);

        setPanDmx(panDmxEvaluated);
        setTiltDmx(tiltDmxEvaluated);

        if (onDevicePositionChangeListener != null) onDevicePositionChangeListener.devicePositionChanged(this);
    }

    /**
     * Set a value for the pan DMX channel
     *
     * @param dmxValue DMX value
     */
    private void setPanDmx(int dmxValue) {
        if (invertedPan) dmxValue = 255 - dmxValue;
        AppData.telnet.sendDmxValue(panChannel, dmxValue);
    }

    /**
     * Set a value for the tilt DMX channel
     *
     * @param dmxValue DMX value
     */
    private void setTiltDmx(int dmxValue) {
        if (invertedTilt) dmxValue = 255 - dmxValue;
        AppData.telnet.sendDmxValue(tiltChannel, dmxValue);
    }

    /**
     * @return Last (logged) position of the device
     */
    int[] getLastPos() {
        return lastPos;
    }

    //The methods below are called when setting up the device to identify whether the device is inverted or not.

    /**
     * Checks whether a device is inverted in DMX control or not
     * Moves device to a random position. ([1;254] as 0 and 255 are the most used default values)
     * DMX-Control will return inverted values on an inverted device, which means the device is inverted
     */
    void checkInverted() {
        invertPanCheckNumber = (int) ((Math.random() * 253) + 1);
        invertTiltCheckNumber = (int) ((Math.random() * 253) + 1);

        checkingInvertPan = true;
        setPanDmx(invertPanCheckNumber);
        checkingInvertTilt = true;
        setTiltDmx(invertTiltCheckNumber);
    }

    /**
     * Called on a response from DMX control after checkInverted
     *
     * @param channel DMX channel
     * @param value DMX value
     * @param type DMX input type (CV, CVC) only CVC is important, as CV returns the same number you set
     */
    private void checkInvertResponse(int channel, int value, int type) {
        if (type == CVC) {
            if (channel == panChannel) {
                if (value == 255 - invertPanCheckNumber) {
                    invertedPan = true;
                    System.out.println(name + " is pan inverted");
                }
                checkingInvertPan = false;
            } else if (channel == tiltChannel) {
                if (value == 255 - invertTiltCheckNumber) {
                    invertedTilt = true;
                    System.out.println(name + " is tilt inverted");
                }
                checkingInvertTilt = false;
            }

            //After checking invert device will move to room coordinate O(0,0,0)
            if (!checkingInvertPan && !checkingInvertTilt) moveTo(new int[]{0, 0, 0});
        }
    }


    //All method below are used to handle device configuration

    /**
     * Saves the device configuration
     * Is always called, even after loading the configuration (to save missing values (when using old versions) and to import settings from another device)
     */
    private void doSave() {
        File file = new File(AppData.configPath + "/Devices/" + Device.this.name + ".ini");

        //Creates the settings file if the file does not exist yet
        if (!file.exists()) {
            try {
                if (!file.createNewFile()) JOptionPane.showConfirmDialog(null,
                        "Datei konnte nicht erstellt werden. Bitte überprüfe Berechtigunegen für: " + file.getAbsolutePath(),
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION);
            } catch (IOException e1) {
                JOptionPane.showConfirmDialog(null,
                        "Datei konnte nicht erstellt werden. Bitte überprüfe Berechtigunegen für: " + file.getAbsolutePath(),
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION);
            }
        }

        // Saves configuration to the file
        // NOTE: Do not remove the if(file.exist()). It seems obsolete at first, but it's useful in the case the file can't be created (code above)
        if (file.exists()) {
            Properties prop = new Properties();
            OutputStream output;

            //Save settings in the file
            try {
                output = new FileOutputStream(file);

                prop.setProperty("xPos", String.valueOf(xPos));
                prop.setProperty("yPos", String.valueOf(yPos));
                prop.setProperty("zPos", String.valueOf(zPos));

                StringBuilder rotationAxisBuilder = new StringBuilder();

                for (int rotationAxisNow : rotationAxis) {
                    rotationAxisBuilder.append(rotationAxisNow).append(";");
                }

                StringBuilder rotationDegreeBuilder = new StringBuilder();

                for (int rotationDegreeNow : rotationDegrees) {
                    rotationDegreeBuilder.append(rotationDegreeNow).append(";");
                }

                prop.setProperty("rotationAxis", rotationAxisBuilder.toString());
                prop.setProperty("rotationDegree", rotationDegreeBuilder.toString());

                prop.setProperty("panDegree", String.valueOf(panDegree));
                prop.setProperty("tiltDegree", String.valueOf(tiltDegree));

                prop.setProperty("formulaAlpha", alphaString);
                prop.setProperty("formulaBeta", betaString);

                prop.store(output, "Do not change manually");

                output.close();
                output.flush();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        //Create a rotation matrix with changed values
        createFinalRotationMatrix();
    }

    /**
     * Creates the final rotation matrix (from multiple rotations)
     */
    private void createFinalRotationMatrix() {
        //TODO: Überprüfen warum rückwärts
        List<int[][]> matrices = new ArrayList<>();

        // for-loop backwards, because it needs to be D = ...*D3*D2*D1
        // Creates a list of all rotation matrices
        for (int i = rotationDegrees.size() - 1; i >= 0; i--) {
            matrices.add(Maths.createRotationMatrix(rotationAxis.get(i), rotationDegrees.get(i), true));
        }

        //Multiply matrices
        int[][] tempRotationMatrix = matrices.get(0);
        for (int i = 0; i < matrices.size() - 1; i++) {
            tempRotationMatrix = Maths.multiplyTwoMatrices(tempRotationMatrix, matrices.get(i + 1));
        }
        rotationMatrix = tempRotationMatrix;
    }

    /**
     * Called to setup a device
     *
     * @return Setup successful
     */
    boolean setup() {
        return setup(false, new File(AppData.configPath + "/Devices/" + Device.this.name + ".ini"));
    }

    /**
     *
     * @param agreeOnSetup if a user wants to import settings from another device he has to agree on the settings first
     * @param file path of the settings file
     * @return Setup successful
     */
    private boolean setup(boolean agreeOnSetup, File file) {
        final int xPosTemp, yPosTemp, zPosTemp, panDegreeTemp, tiltDegreeTemp;
        String[] rotationAxisTemp, rotationDegreeTemp;
        String alphaStringTemp, betaStringTemp;

        boolean successful = false;

        //Make sure the file exists
        if (file.exists()) {
            successful = true;
            Properties prop = new Properties();
            final InputStream input;
            try {
                //Load settings from the file
                input = new FileInputStream(file);
                prop.load(input);
                input.close();
                xPosTemp = Integer.valueOf(prop.getProperty("xPos").trim());
                yPosTemp = Integer.valueOf(prop.getProperty("yPos").trim());
                zPosTemp = Integer.valueOf(prop.getProperty("zPos").trim());

                rotationAxisTemp = prop.getProperty("rotationAxis").trim().split(";");
                rotationDegreeTemp = prop.getProperty("rotationDegree").trim().split(";");

                panDegreeTemp = Integer.valueOf(prop.getProperty("panDegree"));
                tiltDegreeTemp = Integer.valueOf(prop.getProperty("tiltDegree"));

                alphaStringTemp = prop.getProperty("formulaAlpha", "a");
                betaStringTemp = prop.getProperty("formulaBeta", "b");

                //Open a user input (in the case of importing files) to make sure the correct device is imported. Otherwise values will directly be set
                if (agreeOnSetup) {
                    int reply = JOptionPane.showConfirmDialog(null,
                            "Bist du dir sicher, dass du die Einstellungen von '" + file.getName().replace(".ini", "") + "' auf '" + name + "' übertragen willst?\n" +
                                    "Dies wird die Einstellungsdaten in eine neue Datei kopieren.\n\n" +
                                    "Folgende Einstellungen werden übernommen:\n" +
                                    "x-Position: " + xPosTemp + "\n" +
                                    "y-Position: " + yPosTemp + "\n" +
                                    "z-Position: " + zPosTemp + "\n" +
                                    "Rotationsachse: " + Arrays.toString(rotationAxisTemp) + " (1:X, 2:Y, 3:Z)\n" +
                                    "Rotationswinkel: " + Arrays.toString(rotationDegreeTemp) + "\n" +
                                    "Max. Pan-Drehung: " + panDegreeTemp + "\n" +
                                    "Max. Tilt-Drehung: " + tiltDegreeTemp + "\n" +
                                    "Formel Alpha: " + alphaStringTemp + "\n" +
                                    "Formel Beta: " + betaStringTemp,
                            "Diese Einstellungen importieren?",
                            JOptionPane.YES_NO_OPTION);
                    if (reply == JOptionPane.NO_OPTION) {
                        return false;
                    }
                }

                //Write temporary values to final values
                xPos = xPosTemp;
                yPos = yPosTemp;
                zPos = zPosTemp;

                rotationAxis.clear();
                rotationDegrees.clear();
                for (String rotationAxisTempNow : rotationAxisTemp) {
                    rotationAxis.add(Integer.parseInt(rotationAxisTempNow.trim()));
                }
                for (String rotationDegreeTempNow : rotationDegreeTemp) {
                    rotationDegrees.add(Integer.parseInt(rotationDegreeTempNow.trim()));
                }

                panDegree = panDegreeTemp;
                tiltDegree = tiltDegreeTemp;

                alphaString = alphaStringTemp;
                betaString = betaStringTemp;

                alphaExpression = new ExpressionBuilder(alphaString).variable("a").build();
                betaExpression = new ExpressionBuilder(betaString).variable("b").build();

                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        updateListModel(true);

                        xPosField.setText("" + xPos);
                        yPosField.setText("" + yPos);
                        zPosField.setText("" + zPos);

                        panRotField.setText(String.valueOf(panDegree));
                        tiltRotField.setText(String.valueOf(tiltDegree));

                        formulaAlphaTextField.setText(alphaString);
                        formulaBetaTextField.setText(betaString);
                    }
                });
                input.close();
                doSave();
            } catch (IOException e) {
                e.printStackTrace();
                successful = false;
            } catch (NumberFormatException e) {
                e.printStackTrace();
                //Error message (Invalid values)
                JOptionPane.showConfirmDialog(null,
                        "Die Konfigurationsdatei (" + file.getAbsolutePath() + ") ist fehlerhaft. Bitte lies die Werte daraus manuell aus und schreibe die Werte manuell in die Geräteeinstellungen.",
                        "Fehler",
                        JOptionPane.DEFAULT_OPTION);
                successful = false;
            }

        }

        //Return device setup state. Returns true when the setup above was successful or when panChannel or tiltChannel equals -1 (which means the device does not have a pan/tilt functionality
        isSetup = panChannel == -1 || tiltChannel == -1 || successful;
        return isSetup;
    }

    /**
     * Called to update the list model of the rotation JList
     *
     * @param alreadyInInvokeLater GUI changes must be called in invokeLater.
     *                             If alreadyInInvokeLater the calling method already is in invokeLater.
     */
    private void updateListModel(boolean alreadyInInvokeLater) {
        if (alreadyInInvokeLater) {
            updateListModelNoInvoke();
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    updateListModelNoInvoke();
                }
            });
        }
    }

    /**
     * Updating the list model of the rotation list
     */
    private void updateListModelNoInvoke() {
        model.clear();
        for (int i = 0; i < rotationAxis.size(); i++) {
            String toAdd = "";
            switch (rotationAxis.get(i)) {
                case xAxis:
                    toAdd += "x-Achse";
                    break;
                case yAxis:
                    toAdd += "y-Achse";
                    break;
                case zAxis:
                    toAdd += "z-Achse";
                    break;
            }
            toAdd += " (" + rotationDegrees.get(i) + "°)";
            model.addElement(toAdd);
        }
    }
}