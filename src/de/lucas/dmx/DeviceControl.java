package de.lucas.dmx;

import de.lucas.dmx.listener.OnDevicePositionChangeListener;

import javax.swing.*;
import java.awt.event.*;

/**
 * This is the main window to control device movements
 */
public class DeviceControl extends JFrame implements OnDevicePositionChangeListener {
    public JComboBox device1ComboBox;
    public JComboBox device2ComboBox;
    private JButton device1Left;
    private JButton device1Down;
    private JButton device1Up;
    private JButton device1Right;
    private JButton device2Up;
    private JButton device2Left;
    private JButton device2Right;
    private JButton device2Down;
    private JPanel panel1;
    private JButton sendCoordinates1;
    private JButton sendCoordinates2;
    private JTextField xCoordinateField1;
    private JTextField xCoordinateField2;
    private JTextField yCoordinateField1;
    private JTextField zCoordinateField1;
    private ActionListener device1Listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Device device = AppData.devices.get(device1ComboBox.getSelectedIndex());
            if (e.getSource().equals(device1Up))
                device.forward();
            else if (e.getSource().equals(device1Down))
                device.backward();
            else if (e.getSource().equals(device1Left))
                device.left();
            else if (e.getSource().equals(device1Right))
                device.right();
        }
    };
    private JTextField yCoordinateField2;
    private JTextField zCoordinateField2;
    private JButton buttonAddRemove1;
    private JButton buttonAddRemove2;
    private ActionListener device2Listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Device device = AppData.devices.get(device2ComboBox.getSelectedIndex());
            if (e.getSource().equals(device2Up))
                device.forward();
            else if (e.getSource().equals(device2Down))
                device.backward();
            else if (e.getSource().equals(device2Left))
                device.left();
            else if (e.getSource().equals(device2Right))
                device.right();
        }
    };


    /**
     * Called to setup the window
     */
    DeviceControl() {
        super("Device Controller");


        setType(Type.POPUP);
        setSize(750, 380);
        setContentPane(panel1);
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setVisible(false);

        device1Up.addActionListener(device1Listener);
        device1Down.addActionListener(device1Listener);
        device1Left.addActionListener(device1Listener);
        device1Right.addActionListener(device1Listener);

        device2Up.addActionListener(device2Listener);
        device2Down.addActionListener(device2Listener);
        device2Left.addActionListener(device2Listener);
        device2Right.addActionListener(device2Listener);

        panel1.setFocusable(true);

        //Adding the key dmx.listener
        panel1.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                handleKeyEvent(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

        //Adding a focus dmx.listener (focus must be on panel1 to handle key events
        panel1.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            }

            /**
             * Called to retrieve focus to the panel
             * Does not retrieve focus when the focus has changed to specific components (like text- or comboBoxes)
             *
             * @param e FocusEvent
             * @see FocusEvent
             */
            @Override
            public void focusLost(FocusEvent e) {
                if (e == null || e.getOppositeComponent() == null) return;
                if (!e.getOppositeComponent().equals(device1ComboBox) && !e.getOppositeComponent().equals(device2ComboBox)
                        && !e.getOppositeComponent().equals(xCoordinateField1)
                        && !e.getOppositeComponent().equals(yCoordinateField1)
                        && !e.getOppositeComponent().equals(zCoordinateField1)

                        && !e.getOppositeComponent().equals(xCoordinateField2)
                        && !e.getOppositeComponent().equals(yCoordinateField2)
                        && !e.getOppositeComponent().equals(zCoordinateField2)
                        )
                    panel1.requestFocusInWindow();
            }
        });

        /*
         * set focus to panel1 after choosing a device
         */
        ActionListener deviceChosen = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel1.requestFocusInWindow();
                if (AppData.devices.size() > 0 && device1ComboBox.getSelectedIndex() != -1 && device2ComboBox.getSelectedIndex() != -1) {
                    Device device1 = AppData.devices.get(device1ComboBox.getSelectedIndex());
                    Device device2 = AppData.devices.get(device2ComboBox.getSelectedIndex());
                    showDevice1Position(device1);
                    showDevice2Position(device2);

                    device1.onDevicePositionChangeListener = DeviceControl.this;
                    device2.onDevicePositionChangeListener = DeviceControl.this;
                }
            }
        };

        //Adding action dmx.listener for comboBoxes (see above)
        device1ComboBox.addActionListener(deviceChosen);
        device2ComboBox.addActionListener(deviceChosen);

        //Sending coordinates to Device 1
        sendCoordinates1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Device device = AppData.devices.get(device1ComboBox.getSelectedIndex());

                device.moveTo(new int[]{
                        Integer.valueOf(xCoordinateField1.getText()),
                        Integer.valueOf(yCoordinateField1.getText()),
                        Integer.valueOf(zCoordinateField1.getText()),
                });
                panel1.requestFocus();
            }
        });
        //Sending coordinates to Device 2
        sendCoordinates2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Device device = AppData.devices.get(device2ComboBox.getSelectedIndex());

                device.moveTo(new int[]{
                        Integer.valueOf(xCoordinateField2.getText()),
                        Integer.valueOf(yCoordinateField2.getText()),
                        Integer.valueOf(zCoordinateField2.getText()),
                });
                panel1.requestFocus();
            }
        });
    }

    /**
     * Called when a key is pressed
     * @param e KeyEvent
     * @see KeyEvent
     */
    private void handleKeyEvent(KeyEvent e) {
        int keyCode = e.getKeyCode();
        switch (keyCode) {
            //Arrow keys are used for the 2nd device
            case KeyEvent.VK_UP:
                device2Listener.actionPerformed(new ActionEvent(device2Up, 0, ""));
                break;
            case KeyEvent.VK_DOWN:
                device2Listener.actionPerformed(new ActionEvent(device2Down, 0, ""));
                break;
            case KeyEvent.VK_LEFT:
                device2Listener.actionPerformed(new ActionEvent(device2Left, 0, ""));
                break;
            case KeyEvent.VK_RIGHT:
                device2Listener.actionPerformed(new ActionEvent(device2Right, 0, ""));
                break;

            //WASD keys are used for the 1st device
            case KeyEvent.VK_W:
                device1Listener.actionPerformed(new ActionEvent(device1Up, 0, ""));
                break;
            case KeyEvent.VK_S:
                device1Listener.actionPerformed(new ActionEvent(device1Down, 0, ""));
                break;
            case KeyEvent.VK_A:
                device1Listener.actionPerformed(new ActionEvent(device1Left, 0, ""));
                break;
            case KeyEvent.VK_D:
                device1Listener.actionPerformed(new ActionEvent(device1Right, 0, ""));
                break;
        }
    }


    /**
     * Shows position of the 1st device in text field
     *
     * @param device getPosition from this device
     */
    private void showDevice1Position(final Device device) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                int pos[] = device.getLastPos();
                xCoordinateField1.setText(String.valueOf(pos[0]));
                yCoordinateField1.setText(String.valueOf(pos[1]));
                zCoordinateField1.setText(String.valueOf(pos[2]));
            }
        });
    }

    /**
     * Shows position of the 2nd device in text field
     *
     * @param device getPosition from this device
     */
    private void showDevice2Position(final Device device) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                int pos[] = device.getLastPos();
                xCoordinateField2.setText(String.valueOf(pos[0]));
                yCoordinateField2.setText(String.valueOf(pos[1]));
                zCoordinateField2.setText(String.valueOf(pos[2]));
            }
        });
    }

    @Override
    public void devicePositionChanged(Device device) {
        int pos = AppData.devices.indexOf(device);
        if (pos == device1ComboBox.getSelectedIndex())
            showDevice1Position(device);
        if (pos == device2ComboBox.getSelectedIndex())
            showDevice2Position(device);
    }
}
