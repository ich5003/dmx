package de.lucas.dmx.listener;


/**
 * Listener called by ControllerThread
 */
public interface OnControllerListener {
    /**
     * This method is called by the Controller Thread and is called when a thread dies because of an Exception
     *
     */
    void threadDied();
}
