package de.lucas.dmx.listener;

import de.lucas.dmx.Device;

public interface OnDevicePositionChangeListener {
    void devicePositionChanged(Device device);
}
