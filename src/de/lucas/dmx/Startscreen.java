package de.lucas.dmx;

import de.lucas.dmx.listener.MobileDeviceListener;
import de.lucas.dmx.listener.OnControllerListener;
import de.lucas.dmx.listener.OnDmxControlInputListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

/**
 * Main screen of the program
 */
public class Startscreen extends JFrame implements OnDmxControlInputListener {
    static OnDmxControlInputListener dmxControlInputListener;
    private static volatile boolean connected = false;
    MobileDeviceManager mobileDeviceManager;
    int devicesConnected = 0;
    private DefaultListModel model;
    private boolean loadDevices = false;
    private int currentDevice = -1;
    private int deviceAmount = 0;
    private JPanel rootPanel;
    private JList list1;
    private JButton loadDevicesButton;
    private volatile JButton connectButton;
    private OnControllerListener disconnectedListener = new OnControllerListener() {
        @Override
        public void threadDied() {
            disconnected();
        }
    };
    private JRadioButton panTiltDevicesRadioButton;
    private JRadioButton allDevicesRadioButton;
    private JButton deviceControlButton;
    private JLabel connectedMobileDevicesLabel;
    private DeviceControl control = new DeviceControl();

    /**
     * Initialize the window and starts all important tasks
     *
     * @throws Exception
     */
    private Startscreen() throws Exception {
        super("DMX");
        mobileDeviceManager = new MobileDeviceManager(new MobileDeviceListener() {
            @Override
            public void deviceConnected() {
                devicesConnected++;
                reloadMobileDeviceNumber();
            }

            @Override
            public void deviceDisconnected() {
                devicesConnected--;
                reloadMobileDeviceNumber();
            }

            private void reloadMobileDeviceNumber() {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        connectedMobileDevicesLabel.setText(devicesConnected + " mobile Geräte verbunden");
                    }
                });
            }
        });
        mobileDeviceManager.start();

        connectButton.setBackground(Color.RED);

        setSize(500, 250);
        attachShutDownHook();
        setContentPane(rootPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);

        dmxControlInputListener = this;

        list1.setCellRenderer(new CellRenderer());
        model = (DefaultListModel) list1.getModel();

        connect();

        loadDevicesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startLoadDevices();
            }
        });
        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect();
            }
        });
        deviceControlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!control.isVisible()) control.setVisible(true);
                else control.toFront();
            }
        });
        list1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (e.getClickCount() == 2) {
                    int index = list1.locationToIndex(e.getPoint());
                    //Open Device Setting screen on double click
                    if (!AppData.devices.get(index).isVisible()) AppData.devices.get(index).setVisible(true);
                    else AppData.devices.get(index).toFront();
                }
            }
        });
    }

    /**
     * Main program method (starts screen)
     *
     * @param args Default arguments
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Startscreen();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        createDirs();
    }

    /**
     * This method creates all important directories
     */
    private static void createDirs() {
        //Create Dirs for all Settings
        File main = new File(AppData.configPath);
        if (!main.isDirectory() && !main.exists()) main.mkdirs();
        File devices = new File(main.getAbsolutePath() + "/Devices");
        if (!devices.isDirectory() && !devices.exists()) devices.mkdir();
    }

    /**
     * Connect to DMX control
     *
     * @see Telnet
     */
    private void connect() {
        AppData.telnet = new Telnet(this, disconnectedListener);
        if (AppData.telnet.connect()) {
            //Starts channel notification (means DMX control sends all value changes to my program
            AppData.telnet.send("StartChannelNotification 1 1024");
            connected();
        }
    }

    /**
     * Called after a successful connect
     *
     * @see Telnet
     */
    private void connected() {
        connected = true;
        connectButton.setEnabled(false);
        connectButton.setBackground(Color.GREEN);
    }

    /**
     * Called on an unexpected disconnect
     *
     * @see Telnet
     */
    private void disconnected() {
        System.err.println("Es trat ein Fehler in der Verbindung auf. Um weitere Fehler zu vermeiden wurde die Verbindung getrennt!");
        connectButton.setEnabled(true);
        connectButton.setBackground(Color.RED);
        AppData.telnet.disconnect();
        connected = false;
        AppData.telnet = null;
    }

    /**
     * Loads device list from DMX control
     */
    private void startLoadDevices() {
        //Show warning message
        int reply = JOptionPane.showConfirmDialog(null,
                "Beim Laden der Geräteliste werden möglicherweiße Effekte gestoppt\n" +
                        "Außerdem werden sich alle Pan/Tilt-fähigen Geräte zum Ursprung drehen\n" +
                        "Soll der Vorgange trotzdem fortgesetzt werden?",
                "Sicher?",
                JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.NO_OPTION) {
            return;
        }
        //Closes all active devices windows (so they are reloaded after a device list reload)
        for (Device device : AppData.devices) {
            if (device != null) {
                device.setVisible(false);
                device.dispose();
            }
        }

        //Clears the device lists and list models
        loadDevices = true;
        model.clear();
        control.device1ComboBox.removeAllItems();
        control.device2ComboBox.removeAllItems();

        //Sends DMX-Control the command to send the device list
        if (AppData.telnet.send("GetAllDevices")) {
            System.out.println("Loading Devices...");
            AppData.devices.clear();
        } else {
            loadDevices = false;
            System.out.println("Loading Devices failed");
        }
    }

    /**
     * Adds a shutdown hook.
     * This is needed to stop threads and disconnect before closing the program
     *
     * @throws Exception
     */
    private void attachShutDownHook() throws Exception {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    AppData.telnet.disconnect();
                    mobileDeviceManager.stopServer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Uses DMXControl inputs
     * @param message message from DMXControl
     *
     * @see OnDmxControlInputListener
     */
    @Override
    public void input(String message) {
        System.out.println(message);
        String[] splitted = message.split(" ");
        switch (splitted[0]) {

            /*
            CVC: Channel Value Changed (called when a usere changes DMX values in DMX control)
            Looks like: 'CVC [CHANNEL] [VALUE]'

            NOTE: Returns inverted values (on inverted devices)
             */
            case "CVC":
                for (Device device : AppData.devices) {
                    if (device == null) break;
                    device.update(Integer.valueOf(splitted[1]), Integer.valueOf(splitted[2]), Device.CVC);
                }
                break;

            /*
            CV: Channel Value (called on a GetChannel or SetChannel request)
            Looks like: 'CV [CHANNEL] [VALUE]'

            Note: returns real values
             */
            case "CV":
                for (Device device : AppData.devices) {
                    if (device == null) break;
                    device.update(Integer.valueOf(splitted[1]), Integer.valueOf(splitted[2]), Device.CV);
                }
                break;

            /*
            DeviceListChanged: Means the device list of DMX-Control changed
            Looks like: 'DeviceListChanged'
             */
            case "DeviceListChanged":
                int reply = JOptionPane.showConfirmDialog(null,
                        "Ger\u00e4teliste hat sich ge\u00e4ndert, soll die Liste aktualisiert werden?",
                        "Reload",
                        JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    startLoadDevices();
                }
                break;


            default:
                /*
                When the type does not equal on of the types above, it means that the input from DMX is the device list
                 */
                if (loadDevices) {
                    if (message.equals(".")) {
                        loadDevices = false;
                        showDevices();
                    } else {
                        int position = Integer.valueOf(splitted[0].replace(".", ""));
                        String name = "";
                        for (int i = 1; i < splitted.length; i++) {
                            name += splitted[i] + " ";
                        }
                        name = name.trim();
                        AppData.devices.add(new Device(position, name));
                    }
                } else {
                    if (message.equals(".")) {
                        currentDevice = currentDevice + 1;
                        checkFinishedLoading();
                    } else if (splitted[0].startsWith("StartAddress"))
                        AppData.devices.get(currentDevice).startAdress =
                                Integer.valueOf(splitted[1]);
                    else if (splitted[0].startsWith("Channels"))
                        AppData.devices.get(currentDevice).endAdress = AppData.devices.get(currentDevice).startAdress + Integer.valueOf(splitted[1]) - 1;
                    else if (splitted[0].startsWith("PanTilt")) {
                        if (splitted[1].equals("Coarse")) {
                            AppData.devices.get(currentDevice).setPanTiltChannel(Integer.valueOf(splitted[2]), Integer.valueOf(splitted[3]));
                        }
                        if (splitted[1].equals("Fine")) {
                            AppData.devices.get(currentDevice).setPanTiltChannel(Integer.valueOf(splitted[2]), Integer.valueOf(splitted[3]), Integer.valueOf(splitted[4]), Integer.valueOf(splitted[5]));
                        }
                    }
                }
                break;
        }
    }

    /**
     * Called after the device list is loaded.
     * Program requests Detailed device information
     */
    private void showDevices() {
        loadDevices = false;
        System.out.println("Devices List loaded");
        System.out.println("Loading Device Details");
        deviceAmount = AppData.devices.size();
        for (int i = 0; i < AppData.devices.size(); i++) {
            Device device = AppData.devices.get(i);
            if (device == null) {
                break;
            }
            currentDevice = 0;
            AppData.telnet.send("GetDeviceInfo " + device.position);
        }
    }

    /**
     * Called after device list and device information is loaded
     */
    private void checkFinishedLoading() {
        if (currentDevice >= deviceAmount) {
            System.out.println("Devices Details loaded");
            StringBuilder noSetup = new StringBuilder();
            for (int i = 0; i < AppData.devices.size(); i++) {
                if (panTiltDevicesRadioButton.isSelected() && (AppData.devices.get(i).panChannel == -1 && AppData.devices.get(i).tiltChannel == -1)) {
                    AppData.devices.remove(i);
                    i--;
                    continue;
                }

                AppData.devices.get(i).checkInverted();
                if (!AppData.devices.get(i).setup()) noSetup.append(AppData.devices.get(i).name).append("\n");
                final int finalI = i;
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        model.addElement(AppData.devices.get(finalI).name);
                        control.device1ComboBox.addItem(AppData.devices.get(finalI).name);
                        control.device2ComboBox.addItem(AppData.devices.get(finalI).name);
                    }
                });
                System.out.println("From " + AppData.devices.get(i).startAdress + " to " + AppData.devices.get(i).endAdress + ": " + AppData.devices.get(i).name + " (Pan " + AppData.devices.get(i).panChannel + "; Tilt " + AppData.devices.get(i).tiltChannel + "; PanFine " + AppData.devices.get(i).panFineChannel + "; TiltFine " + AppData.devices.get(i).tiltFineChannel + ")");
            }

            if (noSetup.toString().length() != 0) JOptionPane.showConfirmDialog(null,
                    "Es konnten keine gültigen Einstellungen für folgende Geräte gefunden werden:\n\n" + noSetup.toString().trim(),
                    "Fehlende Einstellungen",
                    JOptionPane.DEFAULT_OPTION);

            mainLoop:
            for (Device device : AppData.devices) {
                for (Device device2 : AppData.devices) {
                    if (device != device2 && device.name.equals(device2.name)) {
                        JOptionPane.showConfirmDialog(null,
                                "Es existieren mindestens 2 Scheinwerfer mit dem Name '" + device.name + "'.\n" +
                                        "Bitte ändere die Namen der Schweinwerfer in DMX-Control, da es bei gleichen Namen zu Problemen kommen kann",
                                "Gleiche Namen",
                                JOptionPane.DEFAULT_OPTION);
                        break mainLoop;
                    }
                }
            }

            mobileDeviceManager.notifyDeviceListChange();
        }
    }

    /**
     * This cell renderer is used to mark devices without a settings file red
     */
    private static class CellRenderer extends DefaultListCellRenderer {
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (!AppData.devices.get(index).isSetup) {
                c.setBackground(Color.red);
            } else {
                c.setBackground(Color.white);
            }
            if (isSelected)
                c.setForeground(Color.darkGray);
            return c;
        }
    }
}
