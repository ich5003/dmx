package de.lucas.dmx.listener;

/**
 * This interface is used to output data to the console.
 * Some other classes use this to handle specific OutputTypes (see OutputType.java for more information)
 *
 *
 */
public interface OnDmxControlInputListener {

    /**
     * @param inputMessage message from DMX control
     */
    void input(String inputMessage);
}