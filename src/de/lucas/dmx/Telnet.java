package de.lucas.dmx;

import de.lucas.dmx.listener.OnControllerListener;
import de.lucas.dmx.listener.OnDmxControlInputListener;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;

/**
 * Handles the telnet connection between this program and DMX-Control
 */
public class Telnet {
    private volatile InputStream inputStream;
    private volatile boolean running = true;
    private Socket socket;
    private PrintWriter out = null;
    private BufferedReader in = null;
    private Thread reader;
    private OnDmxControlInputListener onDmxControlInputListener;
    private OnControllerListener disconnectListener;
    private Controller controller;

    /**
     * Constructor of Telnet class
     *
     * @param onOutputListener   called to output inputs from DMX-Control to other threads
     * @param disconnectListener Listener is called by Controller when DMX closes telnet connection
     * @see OnDmxControlInputListener
     * @see Controller
     */
    Telnet(OnDmxControlInputListener onOutputListener, OnControllerListener disconnectListener) {
        this.disconnectListener = disconnectListener;
        this.onDmxControlInputListener = onOutputListener;
    }

    /**
     * Connects to DMX-Control
     *
     * @return connection successful
     * @see Telnet.BackgroundReader
     */
    boolean connect() {
        System.out.println("Connecting");
        try {
            //Connect to DMX-Control (Don't change port in DMX control. Default is 2145)
            socket = new Socket("localhost", 2145);
            //Initialize input and output streams
            out = new PrintWriter(socket.getOutputStream(), true);
            inputStream = socket.getInputStream();
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //Start BackgroundReader
            reader = new BackgroundReader();
            reader.start();
            System.out.println("Connected");
            return true;
        } catch (IOException e) {
            System.err.println("Failed to connect");
            return false;
        }

    }

    /**
     * Sends commands to DMX control
     *
     * @param command Command to send to DMX-Control
     * @return sent successful
     */
    boolean send(String command) {
        if (socket != null && socket.isConnected()) {
            System.out.println(command);
            out.println(command);
            return true;
        } else {
            System.err.println(String.format("Konnte Befehl '%s' nicht senden, da keine Verbindung zu DMX Control besteht.", command));
            return false;
        }
    }

    /**
     * Disconnect this program from DMX-Control and stops all running threads (on program stop)
     */
    void disconnect() {
        running = false;
        if (controller != null) {
            controller.running = false;
            try {
                controller.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            if (inputStream != null) inputStream.close();
            if (out != null) out.close();
            if (socket != null) socket.close();
        } catch (IOException ignored) {
            ignored.printStackTrace();
        }
        if (reader != null) {
            try {
                reader.join();
            } catch (InterruptedException ignored) {
            }
        }
    }

    /**
     * Sends DMX value to a specific channel
     *
     * @param channel DMX channel
     * @param value DMX value
     */
    void sendDmxValue(int channel, int value) {
        send("SetChannel " + channel + " " + value);
    }

    /**
     * Checks whether the program is connected to DMX control or not
     */
    boolean isConnected() {
        return socket.isConnected() && !socket.isClosed();
    }

    /**
     * This Thread constantly checks for a new input from DMX-Control
     *
     * @see Thread
     */
    private class BackgroundReader extends Thread {
        @Override
        public void run() {
            controller = new Controller(disconnectListener, this);
            controller.start();
            while (running) {
                try {
                    //Reads input from DMX-control and outputs it
                    String output = in.readLine();
                    if (output == null) {
                        disconnectListener.threadDied();
                        continue;
                    }
                    onDmxControlInputListener.input(output);
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                    disconnectListener.threadDied();
                }
            }
        }
    }

}
