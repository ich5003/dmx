package de.lucas.dmx.listener;


public interface MobileDeviceListener {
    void deviceConnected();

    void deviceDisconnected();
}
