package de.lucas.dmx;

import com.google.gson.*;
import de.lucas.dmx.listener.MobileDeviceListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

class MobileDeviceManager extends Thread {
    private volatile List<BackgroundReader> backgroundReaderList = new ArrayList<>();
    private volatile List<Socket> socketList = new ArrayList<>();
    private volatile List<BufferedReader> bufferedReaderList = new ArrayList<>();
    private volatile List<PrintStream> printStreamList = new ArrayList<>();
    private MobileDeviceListener deviceListener;
    private ServerSocket ss;
    private boolean running = true;

    MobileDeviceManager(MobileDeviceListener deviceListener) {
        this.deviceListener = deviceListener;
    }

    @Override
    public void run() {
        startServer();
    }

    private void startServer() {
        running = true;

        try {
            ss = new ServerSocket(2146);
            while (running)
                acceptNew();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void acceptNew() {
        try {
            Socket currentSocket = ss.accept();
            socketList.add(currentSocket);
            bufferedReaderList.add(new BufferedReader(new InputStreamReader(currentSocket.getInputStream())));
            printStreamList.add(new PrintStream(currentSocket.getOutputStream()));

            backgroundReaderList.add(new BackgroundReader(backgroundReaderList.size()));
            backgroundReaderList.get(backgroundReaderList.size() - 1).start();
            deviceListener.deviceConnected();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    void stopServer() {
        running = false;
        try {
            ss.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < socketList.size(); i++) {
            closeSingleSocket(i);
            backgroundReaderList.get(i).stopped = true;
            try {
                backgroundReaderList.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void closeSingleSocket(int i) {
        try {
            socketList.get(i).close();
            bufferedReaderList.get(i).close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        printStreamList.get(i).close();

        deviceListener.deviceDisconnected();
    }

    private void sendDeviceList(int position) {
        JsonArray array = new JsonArray();
        for (Device device : AppData.devices) {
            array.add(device.name);
        }
        send(Type.DEVICE_LIST, array, position);
    }

    private void moveDevice(JsonObject data) {
        int device = data.get("device").getAsInt();
        int direction = data.get("direction").getAsInt();
        if (AppData.devices.size() <= device && AppData.devices.size() > device) return;
        switch (direction) {
            case Direction.FORWARD:
                AppData.devices.get(device).forward();
                break;
            case Direction.BACKWARD:
                AppData.devices.get(device).backward();
                break;
            case Direction.LEFT:
                AppData.devices.get(device).left();
                break;
            case Direction.RIGHT:
                AppData.devices.get(device).right();
                break;
        }
    }

    void notifyDeviceListChange() {
        send(Type.DEVICE_LIST_CHANGED, null);
    }

    private void sendName(int position) {
        JsonObject nameObject = new JsonObject();
        String name = "";
        try {
            name = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        nameObject.addProperty("name", name);
        send(Type.NAME, nameObject, position);
    }

    private void send(int type, JsonElement data, int position) {
        JsonObject object = new JsonObject();
        object.addProperty("type", type);
        if (data != null) object.add("data", data);
        if (socketList.get(position) != null && socketList.get(position).isConnected() && !socketList.get(position).isClosed())
            printStreamList.get(position).println(object.toString());
    }

    private void send(int type, JsonElement data) {
        JsonObject object = new JsonObject();
        object.addProperty("type", type);
        if (data != null) object.add("data", data);
        for (int i = 0; i < socketList.size(); i++) {
            if (socketList.get(i) != null && socketList.get(i).isConnected() && !socketList.get(i).isClosed())
                printStreamList.get(i).println(object.toString());
        }
    }


    private static class Type {
        static final int GET_DEVICES = 1, MOVE_DEVICE = 2, DEVICE_LIST = 3, DEVICE_LIST_CHANGED = 4, GET_NAME = 5, NAME = 6;
    }

    private static class Direction {
        static final int FORWARD = 0, BACKWARD = 1, LEFT = 2, RIGHT = 3;
    }

    private class BackgroundReader extends Thread {
        int position;
        boolean thisRunning = true;
        boolean stopped = false;

        public BackgroundReader(int position) {
            this.position = position;
        }

        @Override
        public void run() {
            JsonParser parser = new JsonParser();
            while (running && thisRunning) {
                try {
                    String s = bufferedReaderList.get(position).readLine();
                    System.out.println(s);
                    if (s == null) {
                        thisRunning = false;
                        break;
                    }
                    JsonObject object = parser.parse(s).getAsJsonObject();

                    switch (object.get("type").getAsInt()) {
                        case Type.MOVE_DEVICE:
                            moveDevice(object.get("data").getAsJsonObject());
                            break;
                        case Type.GET_DEVICES:
                            sendDeviceList(position);
                            break;
                        case Type.GET_NAME:
                            sendName(position);
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    thisRunning = false;
                    break;
                } catch (IllegalStateException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            if (!stopped) closeSingleSocket(position);
        }
    }
}