package de.lucas.dmx;

/**
 * Includes all Maths needed by the program
 */
class Maths {

    /**
     * Creates a rotation matrix using the degree and the axis
     *
     * @param rotationAxis   rotated about the specific Axis
     * @param rotationDegree the rotation degree
     * @param KoSysRotation  true: Rotate system, not point
     * @return final rotation matrix
     */
    static int[][] createRotationMatrix(int rotationAxis, int rotationDegree, boolean KoSysRotation) {
        if (KoSysRotation) rotationDegree *= -1;
        double rotationRadian = Math.toRadians(rotationDegree);
        int[][] matrix = new int[3][3];

        //Depending on the rotationAxis the calculation is different.
        switch (rotationAxis) {
            case Device.xAxis:
                matrix[0][0] = 1;

                matrix[0][1] = 0;
                matrix[0][2] = 0;

                matrix[1][0] = 0;
                matrix[2][0] = 0;


                matrix[1][1] = ((int) Math.round(Math.cos(rotationRadian)));
                matrix[1][2] = (-(int) Math.round(Math.sin(rotationRadian)));

                matrix[2][1] = ((int) Math.round(Math.sin(rotationRadian)));
                matrix[2][2] = ((int) Math.round(Math.cos(rotationRadian)));
                break;
            case Device.yAxis:
                matrix[1][1] = 1;

                matrix[0][1] = 0;
                matrix[2][1] = 0;

                matrix[1][0] = 0;
                matrix[1][2] = 0;


                matrix[0][0] = ((int) Math.round(Math.cos(rotationRadian)));
                matrix[0][2] = ((int) Math.round(Math.sin(rotationRadian)));

                matrix[2][0] = (-(int) Math.round(Math.sin(rotationRadian)));
                matrix[2][2] = ((int) Math.round(Math.cos(rotationRadian)));
                break;
            case Device.zAxis:
                matrix[2][2] = 1;

                matrix[2][0] = 0;
                matrix[2][1] = 0;

                matrix[0][2] = 0;
                matrix[1][2] = 0;


                matrix[0][0] = ((int) Math.round(Math.cos(rotationRadian)));
                matrix[0][1] = (-(int) Math.round(Math.sin(rotationRadian)));

                matrix[1][0] = ((int) Math.round(Math.sin(rotationRadian)));
                matrix[1][1] = ((int) Math.round(Math.cos(rotationRadian)));
                break;
        }
        return matrix;
    }

    /**
     * Calculates the coordinates of a point in the Room CoSys in the Device CoSys. (Not rotated yet)
     *
     * @param xPos xPosition of the Device
     * @param yPos yPosition of the Device
     * @param zPos zPosition of the Device
     * @param oldCoordinates old coordinates of the point
     * @return coordinates of the point in the device coordinate system
     */
    private static int[] moveToDeviceSystem(int xPos, int yPos, int zPos, int[] oldCoordinates) {
        /*
        (n1)   (o1-xPos)
        (n2) = (o2-yPos)
        (n3)   (o3-zPos)
         */

        int[] newCoordinates = new int[3];
        newCoordinates[0] = oldCoordinates[0] - xPos;
        newCoordinates[1] = oldCoordinates[1] - yPos;
        newCoordinates[2] = oldCoordinates[2] - zPos;
        return newCoordinates;
    }


    /**
     * This method is used to get the coordinates of a point in the rotated Device CoSys (Using the specific rotation matrix)
     *
     * @param oldCoordinates old coordinates of the point
     * @param matrix the rotation matrix to rotate the device coordinate system.
     * @return coordinates of the point in the rotated device coordinate system
     */
    private static int[] rotateToDeviceSystem(int oldCoordinates[], int matrix[][]) {
        //Multiplies the vector with the rotation matrix
        int[] newCoordinates = new int[3];
        for (int i = 0; i < 3; i++) {
            int tempInt = 0;
            for (int j = 0; j < 3; j++) {
                tempInt += oldCoordinates[j] * matrix[i][j];
            }
            newCoordinates[i] = tempInt;
        }
        return newCoordinates;
    }

    /**
     * Convert point of the room coordinate system to the device coordinate system
     *
     * @param xPos xPosition of the Device
     * @param yPos yPosition of the Device
     * @param zPos zPosition of the Device
     * @param oldCoordinates old coordinates of the point (Coordinates in the room coordinate system)
     * @param matrix the rotation matrix to rotate the device coordinate system.
     * @return coordinates of the point in the moved and rotated device coordinate system
     */
    static int[] newCoordinates(int xPos, int yPos, int zPos, int[] oldCoordinates, int[][] matrix) {
        int[] movedCoordinates = moveToDeviceSystem(xPos, yPos, zPos, oldCoordinates);
        return rotateToDeviceSystem(movedCoordinates, matrix);
    }

    /**
     * Calculate alpha (pan)
     * Formula: a=tan^(-1)(y/x)
     *
     * @param coordinates coordinates of the point (in the device coordinate system)
     * @return alpha
     */
    static double getAlpha(int[] coordinates) {
        int xPosLight = coordinates[0];
        int yPosLight = coordinates[1];
        double forTangens = (double) yPosLight / (double) xPosLight;

        return Math.toDegrees(
                Math.atan(forTangens)
        );
    }

    /**
     * Calculate beta (tilt)
     * Formula: b=cos^(-1)(z/sqrt(x²+y²+z²)
     *
     * @param coordinates coordinates of the point (in the device coordinate system)
     * @return beta
     */
    static double getBeta(int[] coordinates) {
        double xPosLight = coordinates[0];
        double yPosLight = coordinates[1];
        double zPosLight = coordinates[2];
        //z Position can't be negative because that means the light is behind the device.
        if (zPosLight < 0) {
            System.err.println("zPosLight ist NEGATIV!");
            zPosLight = 0;
        }
        return
                Math.toDegrees(
                        Math.acos(
                                zPosLight / Math.sqrt(
                                        Math.pow(xPosLight, 2) + Math.pow(yPosLight, 2) + Math.pow(zPosLight, 2)
                                )
                        )
                );
    }

    /**
     * Multiplies two matrices (matrix1*matrix2)
     *
     * @param matrix1 1st Matrix
     * @param matrix2 2nd Matrix
     * @return multiplied matrix
     */
    static int[][] multiplyTwoMatrices(int[][] matrix1, int[][] matrix2) {
        int[][] newMatrix = new int[3][3];
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                int tempInt = 0;
                for (int matrix1ColAnd2Row = 0; matrix1ColAnd2Row < 3; matrix1ColAnd2Row++) {
                    tempInt += matrix1[row][matrix1ColAnd2Row] * matrix2[matrix1ColAnd2Row][col];
                }
                newMatrix[row][col] = tempInt;
            }
        }
        return newMatrix;
    }

    /**
     * Converts a degree into a DMX value
     *
     * @param degree    degree
     * @param maxDegree maxDegree = 255DMX
     * @return DMX value
     */
    static int degreeToDmx(double degree, double maxDegree) {
        double factor = 255d / maxDegree;
        return (int) (factor * degree);
    }
}
